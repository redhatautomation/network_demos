## Return to Demo Menu
 - [Menu of Demos](../README.md)

## Netcommon Playground


## Objective
The ansible.netcommon collection provides a layer of abstraction to normalize automation across multi-vendor nework devices.

## Overview
In this demo we will leverage the netcommon collection to complete common network use cases across Cisco, Juniper, and Arista network devices.

## Use Cases Provided
* Backups and Restores
* Staging upgrades or downloading files
* Command Lines and Configs
* Managing Resource Modules and Single Source of Truth (SSOT)
* Pings

# Demo
1. Run the Network-Netcommon-Backups Job Template.
2. Select the three device groups and provide a name for the branch to save your backup files
3. Review the network device backup files 
#### Git CLI
1. In VSCode terminal
~~~
$ git branch -r
~~~

example output:
~~~
[student@ansible-1 network-demos-repo]$ git branch -r
  origin/initial_config-2024-01-29T173838.717897+0000
  origin/master
~~~

### Checkout branch in vscode terminal
~~~
[student@ansible-1 network-demos-repo]$ git checkout initial_config-2024-01-29T173838.717897
Switched to a new branch 'initial_config-2024-01-29T173838.717897'
~~~
### List the network_backup_files to the branch
~~~
[student@ansible-1 network-demos-repo]$ ls network_backup_files/
rtr1.txt  rtr2.txt  rtr3.txt  rtr4.txt
~~~
### Cat a config file to review
~~~
[student@ansible-1 network-demos-repo]$ cat network_backup_files/rtr1.txt 
Building configuration...

Current configuration : 7798 bytes
!
! Last configuration change at 15:57:02 UTC Mon Jan 29 2024 by ec2-user
!
version 17.6
service timestamps debug datetime msec
service timestamps log datetime msec
~~~
Truncated

### Checkout master branch
Caution, you must change back to the master branch
~~~
git checkout master
~~~

4. Make an out-of-band (OOB) change to rtr1 and save
5. Run the Network-Netcommon-Restore job template (`selecting your backup branch`) and validate the router change was removed.
6. Run the Network-Netcommon-Facts Job template to gather and write the exiting configuration files for single source of truth (SSOT) to the gitea master branch.
7. Use VSCode to review the `host_vars` in the network-demos-repo/network_netcommon_playground
~~~
git pull
~~~
click on facts.yaml for each router to review configs
8. Review the `network_netcommon_facts.yml` playbook
9. Make an OOB change to rtr1  
10. Run the Network-Netcommon-Diff jo-template and locate the OOB change made above
11. Make an addition to the rtr1 tunnel interfaces in host_vars facts.yaml 
* Note not all of the resource modules are enabled in the demo
* Remember to push your change to the gitea repo using VSCode git extension or CLI
~~~
git add all
git commit -m 'rtr1 changed'
git push
~~~
11. Run the Network-Netcommon-Deploy job template to add and `facts.yaml' remove the changes made from the OOB change
12. Validate the changes were made.
13. Run the Network-Netcommon-Ping Job-template

# Key Takeaways
* An alternative to te network.base validated roles when additional customization is desired
* Provides a Multi-vendor automation solution for Ansible
* Additional abstractions to remove vendor nuances and CLI knowledge

## Return to Demo Menu
 - [Menu of Demos](../README.md)


